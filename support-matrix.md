# Gemnasium analyzer support matrix

| Date | GitLab version | Analyzer version|
| --- | --- | --- |
| [2022-05-22](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/tags/v3.0.0) | 15.0 | [3.x](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/blob/master/CHANGELOG.md#v300) |
| [2019-01-04](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/tags/v2.0.0) | 11.6 - 14.10 | [2.x](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/blob/master/CHANGELOG.md#v200) |
| [2018-11-30](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/tags/v1.0.0) | 11.5 | [1.x](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/blob/master/CHANGELOG.md#v100) |
