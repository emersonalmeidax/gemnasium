package flags

import (
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
)

const (
	// TargetDir is the directory to be scanned
	TargetDir = "target-dir"
	// ArtifactDir is the direcotry where we should save the reports to
	ArtifactDir = "artifact-dir"
)

// defaultFlags are the default set of flags which are used by every CLI command
var defaultFlags = []cli.Flag{
	&cli.StringFlag{
		Name:    TargetDir,
		Usage:   "Target directory",
		EnvVars: []string{command.EnvVarTargetDir, command.EnvVarCIProjectDir},
	},
	&cli.StringFlag{
		Name:    ArtifactDir,
		Usage:   "Artifact directory",
		EnvVars: []string{command.EnvVarArtifactDir, command.EnvVarCIProjectDir},
	},
}

// New combines a default set of flags with the given slices of flags and returns them
func New(inFlags ...[]cli.Flag) []cli.Flag {
	combinedFlags := defaultFlags

	for _, flag := range inFlags {
		combinedFlags = append(combinedFlags, flag...)
	}

	return combinedFlags
}
