package golang

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGolangBuilder(t *testing.T) {
	var builder Builder
	gotFileName, _, err := builder.Build("../../qa/fixtures/go-modules/default/go.mod")
	require.NoError(t, err, "running go builder")
	require.Equal(t, "go-project-modules.json", gotFileName)

	got, err := os.ReadFile("../../qa/fixtures/go-modules/default/go-project-modules.json")
	require.NoError(t, err, "reading exported file")

	expect, err := os.ReadFile("../../qa/expect/go-modules/default/go-project-modules.json")
	require.NoError(t, err, "reading expected file")

	require.JSONEq(t, string(expect), string(got), "comparing go project module list")
}
