require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/sbom_manifest_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe "running image" do
  let(:fixtures_dir) { "qa/fixtures" }
  let(:expectations_dir) { "qa/expect" }

  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium-maven:latest")
  end

  context "with no project" do
    before(:context) do
      @output = `docker run -t --rm -w /app #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it "shows there is no match" do
      expect(@output).to match(/no match in \/app/i)
    end

    describe "exit code" do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context "with test project" do
    def parse_expected_report(expectation_name, report_filename: "gl-dependency-scanning-report.json")
      path = File.join(expectations_dir, expectation_name, report_filename)
      JSON.parse(File.read path)
    end

    let(:global_vars) do
      {
        "GEMNASIUM_DB_REF_NAME": "v1.2.142",
        "SECURE_LOG_LEVEL": "debug"
      }
    end

    let(:project) { "any" }
    let(:relative_expectation_dir) { project }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }

    let(:scan) do
      target_dir = File.join(fixtures_dir, project)
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables))
    end

    let(:report) { scan.report }

    shared_examples_for 'a complete scan' do
      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(expected_report_dir) }
        end

        it_behaves_like "valid report"
      end
    end

    context "using maven" do
      let(:project) { "java-maven/default" }
      let(:scanned_files) { ['pom.xml'] }
      let(:expected_report_dir) { 'java-maven/default' }

      context 'default configuration', run_parallel: true do
        it_behaves_like 'a complete scan'
      end

      context 'java 8', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=1.8 -Dmaven.compiler.target=1.8 -DskipTests --batch-mode", "DS_JAVA_VERSION": "8" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 11', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=11 -Dmaven.compiler.target=11 -DskipTests --batch-mode", "DS_JAVA_VERSION": "11" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 13', build: 'debian', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=13 -Dmaven.compiler.target=13 -DskipTests --batch-mode", "DS_JAVA_VERSION": "13" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 14', build: 'debian', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=14 -Dmaven.compiler.target=14 -DskipTests --batch-mode", "DS_JAVA_VERSION": "14" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 15', build: 'debian', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=15 -Dmaven.compiler.target=15 -DskipTests --batch-mode", "DS_JAVA_VERSION": "15" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 16', build: 'debian', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=16 -Dmaven.compiler.target=16 -DskipTests --batch-mode", "DS_JAVA_VERSION": "16" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 17', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=17 -Dmaven.compiler.target=17 -DskipTests --batch-mode", "DS_JAVA_VERSION": "17" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'multimodule' do
        let(:project) { "java-maven/multimodules/default" }
        let(:expected_report_dir) { "java-maven/multimodules/default" }

        context 'default configuration', run_parallel: true do
          let(:scanned_files) { ["api/pom.xml", "model/pom.xml", "pom.xml", "web/pom.xml"] }

          it_behaves_like 'a complete scan'
        end

        context 'project excluded using' do
          let(:expected_report_dir) { "java-maven/multimodules/no-web" }
          let(:scanned_files) { ["api/pom.xml", "model/pom.xml", "pom.xml"] }

          context 'module dir name', run_parallel: true do
            let(:variables) do
              { "DS_EXCLUDED_PATHS": "web" }
            end

            it_behaves_like 'a complete scan'
          end

          context 'exported file name', run_parallel: true do
            let(:variables) do
              { "DS_EXCLUDED_PATHS": "web/gemnasium-maven-plugin.json" }
            end

            it_behaves_like 'a complete scan'
          end

          context 'build file name', run_parallel: true do
            let(:variables) do
              { "DS_EXCLUDED_PATHS": "web/pom.xml" }
            end

            it_behaves_like 'a complete scan'
          end
        end
      end
    end

    context 'using gradle on RedHat (FIPS)', build: 'redhat' do
      let(:project) { "java-gradle/default" }

      describe "exit code" do
        specify { expect(scan.exit_code).to eq 0 }
      end

      it "logs analyzer name and version" do
        expect(scan.combined_output).to match /\[INFO\].*analyzer v/
      end

      it "logs no error" do
        expect(scan.combined_output).not_to match /\[(ERRO|FATA)\]/
      end

      it "does not create a report" do
        expect(File).not_to exist(scan.report_path)
      end

      it "shows a warning saying that Gradle is not supported" do
        expect(scan.combined_output).to match(/\[WARN\].*Gradle.*not supported/)
      end
    end

    context 'using gradle on Debian', build: 'debian' do
      let(:project) { "java-gradle/default" }
      let(:expected_report_dir) { "java-gradle/default" }
      let(:scanned_files) { ['build.gradle'] }

      context 'default configuration', run_parallel: true do
        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected default version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-17|java-17-openjdk)/)
        end
      end

      context 'path to pom excluded', run_parallel: true do
        let(:variables) { {  "DS_EXCLUDED_PATHS": "pom.xml", "DS_JAVA_VERSION": "11" } }
        let(:project) { "java-gradle/with-pom" }

        it_behaves_like 'a complete scan'
      end

      context 'java 8', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 8 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-8|java-1.8.0-openjdk)/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 11', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-11|java-11-openjdk)/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 13', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 13 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'adoptopenjdk-13/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 14', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 14 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'adoptopenjdk-14/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 15', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 15 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'adoptopenjdk-15/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 16', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 16 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'adoptopenjdk-16/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '7\.3\.3'/)
        end
      end

      context 'java 17', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 17 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-17|java-17-openjdk)/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '7\.3\.3'/)
        end
      end

      context 'gradle 5.6', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/gradle-5-6" }

        it_behaves_like 'a complete scan'
      end

      context 'gradle 6.7', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/gradle-6-7" }

        it_behaves_like 'a complete scan'
      end

      context 'gradle 6.9', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/gradle-6-9" }

        it_behaves_like 'a complete scan'
      end

      context 'gradle 7.3', run_parallel: true do
        let(:project) { "java-gradle/gradle-7-3" }

        it_behaves_like 'a complete scan'
      end

      context 'kotlin dsl', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/kotlin-dsl" }
        let(:scanned_files) { ['build.gradle.kts'] }
        let(:expected_report_dir) { 'java-gradle/kotlin-dsl' }

        it_behaves_like 'a complete scan'
      end

      context 'multimodule' do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/multimodules/default" }
        let(:scanned_files) { ["api/build.gradle", "build.gradle", "model/build.gradle", "web/build.gradle"] }
        let(:expected_report_dir) { "java-gradle/multimodules/default" }

        context 'default configuration', run_parallel: true do
          it_behaves_like 'a complete scan'
        end

        context 'customized buildfile names', run_parallel: true do
          let(:project) { "java-gradle/multimodules/subprojects-buildfilename" }

          it_behaves_like 'a complete scan'
        end

        context 'no root dependencies', run_parallel: true do
          let(:project) { "java-gradle/multimodules/no-root-dependencies" }
          let(:expected_report_dir) { "java-gradle/multimodules/no-root-dependencies" }
          let(:scanned_files) { ["api/build.gradle", "model/build.gradle", "web/build.gradle"] }
          let(:vulnerable_files) { ["api/build.gradle", "web/build.gradle"] }

          it_behaves_like 'a complete scan'
        end
      end

      context "with build containing dependency with circular references", run_parallel: true do
        let(:recorded_report) { parse_expected_report("java-gradle/dependency-with-circular-references/implementation-directive") }

        context "and using the implementation directive" do
          let(:project) { "java-gradle/dependency-with-circular-references/implementation-directive" }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report"
            it_behaves_like "valid report"
          end
        end

        context "and using the compile directive which requires gradle <= 6 and Java <= 15" do
          let(:variables) { { "DS_JAVA_VERSION": "11" } }
          let(:project) { "java-gradle/dependency-with-circular-references/compile-directive" }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report"
            it_behaves_like "valid report"
          end
        end
      end

      context "cyclone dx" do
        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["gl-sbom-maven-gradle.cdx.json"] }

          it_behaves_like "expected CycloneDX metadata tool-name", "Gemnasium"

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end

      context "using sbom command" do
        let(:script) do
          <<-HERE
#!/bin/sh

/analyzer sbom
          HERE
        end

        it "does not create a report" do
          expect(File).not_to exist(scan.report_path)
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["gl-sbom-maven-gradle.cdx.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end

        context "and setting ADDITIONAL_CA_CERT_BUNDLE" do
          let(:variables) do
            { ADDITIONAL_CA_CERT_BUNDLE: <<-HERE
-----BEGIN CERTIFICATE-----
MIIFgzCCBSigAwIBAgIQBiPZw4be7paOmVGMBVHSLDAKBggqhkjOPQQDAjBKMQsw
CQYDVQQGEwJVUzEZMBcGA1UEChMQQ2xvdWRmbGFyZSwgSW5jLjEgMB4GA1UEAxMX
Q2xvdWRmbGFyZSBJbmMgRUNDIENBLTMwHhcNMjIwNzA0MDAwMDAwWhcNMjIxMDAy
MjM1OTU5WjBqMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQG
A1UEBxMNU2FuIEZyYW5jaXNjbzEZMBcGA1UEChMQQ2xvdWRmbGFyZSwgSW5jLjET
MBEGA1UEAxMKZ2l0bGFiLmNvbTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABACu
ir0FpY/Td6QwLGHdBMssRJ9jEvqFqYj2y6K9ZQkiKbgzITxLPzUX5it8/ctNGiqn
+KOhQQefGKkRWYTzs5qjggPOMIIDyjAfBgNVHSMEGDAWgBSlzjfq67B1DpRniLRF
+tkkEIeWHzAdBgNVHQ4EFgQUp7B0Ubbe4FBSgpW+ucHTDarKhd4wgZQGA1UdEQSB
jDCBiYITcGFja2FnZXMuZ2l0bGFiLmNvbYIUY3VzdG9tZXJzLmdpdGxhYi5jb22C
CmdpdGxhYi5jb22CD2NoZWYuZ2l0bGFiLmNvbYIaZW1haWwuY3VzdG9tZXJzLmdp
dGxhYi5jb22CDmthcy5naXRsYWIuY29tghNyZWdpc3RyeS5naXRsYWIuY29tMA4G
A1UdDwEB/wQEAwIHgDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwewYD
VR0fBHQwcjA3oDWgM4YxaHR0cDovL2NybDMuZGlnaWNlcnQuY29tL0Nsb3VkZmxh
cmVJbmNFQ0NDQS0zLmNybDA3oDWgM4YxaHR0cDovL2NybDQuZGlnaWNlcnQuY29t
L0Nsb3VkZmxhcmVJbmNFQ0NDQS0zLmNybDA+BgNVHSAENzA1MDMGBmeBDAECAjAp
MCcGCCsGAQUFBwIBFhtodHRwOi8vd3d3LmRpZ2ljZXJ0LmNvbS9DUFMwdgYIKwYB
BQUHAQEEajBoMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5kaWdpY2VydC5jb20w
QAYIKwYBBQUHMAKGNGh0dHA6Ly9jYWNlcnRzLmRpZ2ljZXJ0LmNvbS9DbG91ZGZs
YXJlSW5jRUNDQ0EtMy5jcnQwDAYDVR0TAQH/BAIwADCCAX0GCisGAQQB1nkCBAIE
ggFtBIIBaQFnAHUAKXm+8J45OSHwVnOfY6V35b5XfZxgCvj5TV0mXCVdx4QAAAGB
xronxwAABAMARjBEAiA5IrJ05KRTZhpfPx4F0q+4fPYuXsURNIwl2+IDIXGfPgIg
GNMXcN5Ls2LPn5I8FJQ5PRzG+N/ipXIKym3BaHLNnP8AdgBByMqx3yJGShDGoToJ
QodeTjGLGwPr60vHaPCQYpYG9gAAAYHGuigJAAAEAwBHMEUCIE2XyQm+tVEVrmPk
qOTjT4LZR1cjzflhSWd6Gm9ydHG6AiEArEldk9K6kKLCLr1TsniI5FbyDoG86s83
+O1XPL+5swkAdgDfpV6raIJPH2yt7rhfTj5a6s2iEqRqXo47EsAgRFwqcwAAAYHG
uifLAAAEAwBHMEUCIAtAsPWw7ExMrIDeYiFYLuckjrqyTl4aRdVf3EdmgXFiAiEA
gMfVbiysb2r+G6pqUIm3IKZf1qzUCMgcQE0QDxd02mgwCgYIKoZIzj0EAwIDSQAw
RgIhAJq7U7Fasv+fIk/j1dlplJxovxE3YQTThZ/WnGmylkt/AiEAmLbVsjNPtKZW
sSwAYSAKFTsYqEWJLHbP9zi2dCvHtH4=
-----END CERTIFICATE-----
HERE
 }
          end

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["gl-sbom-maven-gradle.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end
      end

      context "with build using implementation directive and nested dependencies", run_parallel: true do
        let(:project) { "java-gradle/nested-dependencies-with-implementation-directive" }
        let(:expected_report_dir) { "java-gradle/nested-dependencies-with-implementation-directive" }
        let(:variables) { { "GEMNASIUM_DB_REF_NAME": "v2.0.749" } }

        it_behaves_like 'a complete scan'
      end
    end

    context 'using scala and sbt' do
      let(:project) { "scala-sbt/default" }
      let(:expected_report_dir) { "scala-sbt/default" }
      let(:scanned_files) { ["build.sbt"] }

      context 'default configuration', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'java 8', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 8 } }

        it_behaves_like 'a complete scan'
      end

      context 'java 11', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.0', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.0.4", "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.1', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.1.6", "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.2', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.2.8", "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.3', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.3.12" } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.4', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.4.6" } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.5', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.5.8" } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.6', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.6.1" } }

        it_behaves_like 'a complete scan'
      end

      context 'multiproject', run_parallel: true  do
        let(:variables) { { "DS_JAVA_VERSION": "11" } }
        let(:project) { "scala-sbt/multiproject/default" }
        let(:expected_report_dir) { "scala-sbt/multiproject/default" }
        let(:scanned_files) { ["build.sbt", "proj1/build.sbt", "proj2/build.sbt"] }

        it_behaves_like 'a complete scan'

        describe "SBOM Manifest" do
          let(:relative_sbom_manifest_path) { "sbom-manifest.json" }

          it_behaves_like "non-empty SBOM manifest"
          it_behaves_like "recorded SBOM manifest"
        end
      end
    end
  end
end
