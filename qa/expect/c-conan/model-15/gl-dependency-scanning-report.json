{
  "version": "15.0.0",
  "vulnerabilities": [
    {
      "id": "a331d39cfdaf9e843bb40da2e9b72ac904b4d789f8abfeb9c46e81310f260605",
      "name": "Reachable Assertion",
      "description": "JerryScript allows attackers to cause a denial of service (assertion failure) because a property key query for a Proxy object returns unintended data.",
      "severity": "High",
      "solution": "Upgrade to version 2.3.0 or above.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "jerryscript"
          },
          "version": "2.2.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-30e927ab-bba0-4585-a3b8-892baf51676e",
          "value": "30e927ab-bba0-4585-a3b8-892baf51676e",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/jerryscript/CVE-2020-13622.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-13622",
          "value": "CVE-2020-13622",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-13622"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-13622"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "jerryscript:2.2.0"
        }
      }
    },
    {
      "id": "7f2a4f8e90719c6cc3435f349ed00b61fc97e90351e89e47ea945b12efa2fd88",
      "name": "Uncontrolled Resource Consumption",
      "description": "JerryScript allows attackers to cause a denial of service (stack consumption) via a proxy operation.",
      "severity": "High",
      "solution": "Upgrade to version 2.3.0 or above.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "jerryscript"
          },
          "version": "2.2.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-1dbd122e-b5ab-4cf0-a81b-545500fcfd5e",
          "value": "1dbd122e-b5ab-4cf0-a81b-545500fcfd5e",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/jerryscript/CVE-2020-13623.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-13623",
          "value": "CVE-2020-13623",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-13623"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-13623"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "jerryscript:2.2.0"
        }
      }
    },
    {
      "id": "108f8c96df2850835b57074a8523948ef8b7bc51b5f8050baff02f3e3332c819",
      "name": "NULL Pointer Dereference",
      "description": "`parser/js/js-scanner.c` in JerryScript mishandles errors during certain out-of-memory conditions, as demonstrated by a `scanner_reverse_info_list` NULL pointer dereference and a `scanner_scan_all` assertion failure.",
      "severity": "High",
      "solution": "Upgrade to version 2.3.0 or above.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "jerryscript"
          },
          "version": "2.2.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-e38493c1-c8ba-40b7-9d94-ea7fec727336",
          "value": "e38493c1-c8ba-40b7-9d94-ea7fec727336",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/jerryscript/CVE-2020-13649.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-13649",
          "value": "CVE-2020-13649",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-13649"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-13649"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "jerryscript:2.2.0"
        }
      }
    },
    {
      "id": "6066391f2e164e1682a440a534a841c10d2e136f6df8cadcca95e04572f33af6",
      "name": "Improper Restriction of Operations within the Bounds of a Memory Buffer",
      "description": "An issue was discovered in `ecma/operations/ecma-container-object.c` in JerryScript. Operations with key/value pairs did not consider the case where garbage collection is triggered after the key operation but before the value operation, as demonstrated by improper read access to memory in `ecma_gc_set_object_visited` in `ecma/base/ecma-gc.c`.",
      "severity": "High",
      "solution": "Upgrade to version 2.3.0 or above.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "jerryscript"
          },
          "version": "2.2.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-013a038e-8865-4f92-aedc-0796684481d4",
          "value": "013a038e-8865-4f92-aedc-0796684481d4",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/jerryscript/CVE-2020-14163.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-14163",
          "value": "CVE-2020-14163",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-14163"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-14163"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "jerryscript:2.2.0"
        }
      }
    },
    {
      "id": "db9d56971fb3e5037876dd4e3b0527af975998ad838d050a7f10591c02e55150",
      "name": "Out-of-bounds Read",
      "description": "JerryScript is vulnerable to a buffer over-read.",
      "severity": "High",
      "solution": "Unfortunately, there is no solution available yet.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "jerryscript"
          },
          "version": "2.2.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-7c4e8b05-e4e2-43fc-8fea-665da419fd04",
          "value": "7c4e8b05-e4e2-43fc-8fea-665da419fd04",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/jerryscript/CVE-2020-24344.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-24344",
          "value": "CVE-2020-24344",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-24344"
        }
      ],
      "links": [
        {
          "url": "https://github.com/jerryscript-project/jerryscript/issues/3976"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-24344"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "jerryscript:2.2.0"
        }
      }
    },
    {
      "id": "1275036c0bd873dd8a322758edd5f0565a4313e4ee8d86e8df1d0b1264d1fb3e",
      "name": "Out-of-bounds Write",
      "description": "JerryScript allows stack consumption via `function a(){new new Proxy(a,{})}JSON.parse(\"[]\",a).`",
      "severity": "High",
      "solution": "Unfortunately, there is no solution available yet.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "jerryscript"
          },
          "version": "2.2.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-57af9db1-0aaf-482d-b0b5-1ea4f2027ca0",
          "value": "57af9db1-0aaf-482d-b0b5-1ea4f2027ca0",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/jerryscript/CVE-2020-24345.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-24345",
          "value": "CVE-2020-24345",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-24345"
        }
      ],
      "links": [
        {
          "url": "https://github.com/jerryscript-project/jerryscript/issues/3977"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-24345"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "jerryscript:2.2.0"
        }
      }
    },
    {
      "id": "b9d70b704967b06ee2dd06b6cb6a4c13fb6f23a1996cbf479555516134dd3f04",
      "name": "Improper Input Validation",
      "description": "wolfSSL mishandles the change_cipher_spec (CCS) message processing logic for TLS 1.3. If an attacker sends `ChangeCipherSpec` messages in a crafted way involving more than one in a row, the server becomes stuck in the `ProcessReply()` loop, i.e., a denial of service.",
      "severity": "High",
      "solution": "Unfortunately, there is no solution available yet.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "wolfssl"
          },
          "version": "4.4.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-3ecb7d77-2505-4ba8-8598-781d7bbfeaf8",
          "value": "3ecb7d77-2505-4ba8-8598-781d7bbfeaf8",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/wolfssl/CVE-2020-12457.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-12457",
          "value": "CVE-2020-12457",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-12457"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-12457"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "wolfssl:4.4.0"
        }
      }
    },
    {
      "id": "422a1deda6a6eff889d3c66e062f8b5f63d7b37c13ebd24ccff051ed05feddc5",
      "name": "Concurrent Execution using Shared Resource with Improper Synchronization (Race Condition)",
      "description": "An issue was discovered in wolfSSL when single precision is not employed. Local attackers can conduct a cache-timing attack against public key operations. These attackers may already have obtained sensitive information if the affected system has been used for private key operations (e.g., signing with a private key).",
      "severity": "High",
      "solution": "Unfortunately, there is no solution available yet.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "wolfssl"
          },
          "version": "4.4.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-f23a47a2-7a1d-46fa-a245-f9d92b1e7e2a",
          "value": "f23a47a2-7a1d-46fa-a245-f9d92b1e7e2a",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/wolfssl/CVE-2020-15309.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-15309",
          "value": "CVE-2020-15309",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-15309"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-15309"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "wolfssl:4.4.0"
        }
      }
    },
    {
      "id": "648a79328c0c29ecd05ea98e30ada95938f61043db4abb4c71adb5f25f59ccd2",
      "name": "Cryptographic Issues",
      "description": "An issue was discovered in the DTLS handshake implementation in wolfSSL. Clear DTLS application_data messages in epoch 0 do not produce an out-of-order error. Instead, these messages are returned to the application.",
      "severity": "Medium",
      "solution": "Unfortunately, there is no solution available yet.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "wolfssl"
          },
          "version": "4.4.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-e93de5b9-5b92-465c-a764-4851ad6920b0",
          "value": "e93de5b9-5b92-465c-a764-4851ad6920b0",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/wolfssl/CVE-2020-24585.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-24585",
          "value": "CVE-2020-24585",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-24585"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-24585"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "wolfssl:4.4.0"
        }
      }
    },
    {
      "id": "b901b562c53d560bf754dbd08b0e58c795f7e79e455acd68130712eb39fe1dfd",
      "name": "Improper Certificate Validation",
      "description": "wolfSSL mishandles TLS server data in the `WAIT_CERT_CR` state, within `SanityCheckTls13MsgReceived()` in `tls13.c`. This is an incorrect implementation of the TLS client state machine. This allows attackers in a privileged network position to completely impersonate any TLS servers, and read or modify potentially sensitive information between clients using the wolfSSL library and these TLS servers.",
      "severity": "Medium",
      "solution": "Unfortunately, there is no solution available yet.",
      "location": {
        "file": "conan.lock",
        "dependency": {
          "package": {
            "name": "wolfssl"
          },
          "version": "4.4.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-f334f11b-6d5d-4654-97b6-101f6faa0ad3",
          "value": "f334f11b-6d5d-4654-97b6-101f6faa0ad3",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/conan/wolfssl/CVE-2020-24613.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-24613",
          "value": "CVE-2020-24613",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-24613"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-24613"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "wolfssl:4.4.0"
        }
      }
    }
  ],
  "dependency_files": [
    {
      "path": "conan.lock",
      "package_manager": "conan",
      "dependencies": [
        {
          "package": {
            "name": "jerryscript"
          },
          "version": "2.2.0"
        },
        {
          "package": {
            "name": "wolfssl"
          },
          "version": "4.4.0"
        },
        {
          "package": {
            "name": "zstd"
          },
          "version": "1.4.4"
        }
      ]
    }
  ],
  "scan": {
    "analyzer": {
      "id": "gemnasium",
      "name": "Gemnasium",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "3.6.0"
    },
    "scanner": {
      "id": "gemnasium",
      "name": "Gemnasium",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "3.6.0"
    },
    "type": "dependency_scanning",
    "start_time": "2022-08-08T12:42:48",
    "end_time": "2022-08-08T12:42:52",
    "status": "success"
  }
}
